package com.sample.foo.simplelocationapp.io;

import com.sample.foo.simplelocationapp.models.InstallApp;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by desarrollo on 17/08/2017.
 */

public interface ApiService {
    @GET("installapp")
    Call<ArrayList<InstallApp>> getInstallApp(
            @Query("app_cel_number") String app_cel_number
    );

    @FormUrlEncoded
    @POST("installapp")
    public Call<ResponseBody> insertInstallApp(
            @Field("app_id") int app_id,
            @Field("app_version") double app_version,
            @Field("app_sysope") int app_sysope,
            @Field("app_imac_number") String app_imac_number,
            @Field("app_cel_number") String app_cel_number,
            @Field("app_new_date") String app_new_date,
            @Field("app_new_location") String app_new_location);


    @FormUrlEncoded
    @POST("trackingmessage")
    public Call<ResponseBody> insertTrackingMessage(
            @Field("mservice_id") int mservice_id,
            @Field("message_type") int message_type,
            @Field("message_subtype") int message_subtype,
            @Field("message_message") String message_message,
            @Field("message_new_date") String message_new_date,
            @Field("message_new_location") String message_new_location)
    ;
}
