package com.sample.foo.simplelocationapp.io.response;

import com.sample.foo.simplelocationapp.models.InstallApp;
import java.util.ArrayList;

/**
 * Created by desarrollo on 17/08/2017.
 */

public class InstallAppResponse {
    private ArrayList<InstallApp> results;

    public ArrayList<InstallApp> getResults() {
        return results;
    }

    public void setResults(ArrayList<InstallApp> results) {

        this.results = results;
    }

}
