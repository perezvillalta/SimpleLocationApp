package com.sample.foo.simplelocationapp.models;

/**
 * Created by desarrollo on 17/08/2017.
 */

public class InstallApp {
    private int app_serialnumber; //	int(11) Incremento automático
    private int app_id; //	int(11) NULL
    private Double app_version; //	decimal(3,2) NULL
    private int app_sysope; //	int(11) NULL	1 => Android 2=> IOS 3=>W
    private String app_imac_number; //	varchar(45) NULL
    private String app_cel_number; //	varchar(45) NULL
    private String app_upd_date;	//datetime NULL
    private String app_upd_location; //	text NULL
    private String app_new_date;	//datetime NULL	Fecha de creacion
    private String app_new_location; //	text NULL

    public int getApp_serialnumber() {
        return app_serialnumber;
    }

    public void setApp_serialnumber(int app_serialnumber) {
        this.app_serialnumber = app_serialnumber;
    }

    public int getApp_id() {
        return app_id;
    }

    public void setApp_id(int app_id) {
        this.app_id = app_id;
    }

    public Double getApp_version() {
        return app_version;
    }

    public void setApp_version(Double app_version) {
        this.app_version = app_version;
    }

    public int getApp_sysope() {
        return app_sysope;
    }

    public void setApp_sysope(int app_sysope) {
        this.app_sysope = app_sysope;
    }

    public String getApp_imac_number() {
        return app_imac_number;
    }

    public void setApp_imac_number(String app_imac_number) {
        this.app_imac_number = app_imac_number;
    }

    public String getApp_cel_number() {
        return app_cel_number;
    }

    public void setApp_cel_number(String app_cel_number) {
        this.app_cel_number = app_cel_number;
    }

    public String getApp_upd_date() {
        return app_upd_date;
    }

    public void setApp_upd_date(String app_upd_date) {
        this.app_upd_date = app_upd_date;
    }

    public String getApp_upd_location() {
        return app_upd_location;
    }

    public void setApp_upd_location(String app_upd_location) {
        this.app_upd_location = app_upd_location;
    }

    public String getApp_new_date() {
        return app_new_date;
    }

    public void setApp_new_date(String app_new_date) {
        this.app_new_date = app_new_date;
    }

    public String getApp_new_location() {
        return app_new_location;
    }

    public void setApp_new_location(String app_new_location) {
        this.app_new_location = app_new_location;
    }
}
