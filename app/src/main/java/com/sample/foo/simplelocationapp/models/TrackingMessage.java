package com.sample.foo.simplelocationapp.models;

/**
 * Created by desarrollo on 24/08/2017.
 */

public class TrackingMessage {
    private int message_id;                 //int(11) Incremento automático
    private int  mservice_id;               //int(11)
    private int message_type;               //int(11) NULL
    private int message_subtype;            //int(11) NULL
    private String message_message;         //text NULL
    private String message_new_date;        //datetime NULL
    private String message_new_location;    //text NULL

    public int getMessage_id() {
        return message_id;
    }

    public void setMessage_id(int message_id) {
        this.message_id = message_id;
    }

    public int getMservice_id() {
        return mservice_id;
    }

    public void setMservice_id(int mservice_id) {
        this.mservice_id = mservice_id;
    }

    public int getMessage_type() {
        return message_type;
    }

    public void setMessage_type(int message_type) {
        this.message_type = message_type;
    }

    public int getMessage_subtype() {
        return message_subtype;
    }

    public void setMessage_subtype(int message_subtype) {
        this.message_subtype = message_subtype;
    }

    public String getMessage_message() {
        return message_message;
    }

    public void setMessage_message(String message_message) {
        this.message_message = message_message;
    }

    public String getMessage_new_date() {
        return message_new_date;
    }

    public void setMessage_new_date(String message_new_date) {
        this.message_new_date = message_new_date;
    }

    public String getMessage_new_location() {
        return message_new_location;
    }

    public void setMessage_new_location(String message_new_location) {
        this.message_new_location = message_new_location;
    }
}
